#include<TEveManager.h>
#include<TGLViewer.h>
#include<TRandom.h>
#include<TSystem.h>
#include<TRint.h>
#include<TThread.h>
#include<TEveLine.h>
#include<TGFrame.h>
#include<TFile.h>
#include<TTree.h>
#include<TVector3.h>
#include<vector>
#include<list>
//#include<GL/glut.h>



class Link;


class Node{
	public:
	float v[3]; // values for hash table
	void* data;
	
	std::vector<Link*> links;
	
	Node(){};
	
	void addLink(Link* l);
	void removeLink(Link* l);
	std::vector<Link*> getLinks(int flag, int direction=0);
	static bool compareNodePtr(Node* n1, Node* n2);
	void set(float v0, float v1, float v2) {v[0] = v0; v[1] = v1; v[2] = v2;} 
	void* getData() { return data; }
	virtual int ID(){ return 0; }
	//Oscar: adding Plate()
	virtual int Plate(){ return 0; }
	
};

bool operator<(const Node& n1, const Node& n2) { return n1.v[0] < n2.v[0];}
bool operator>(const Node& n1, const Node& n2) { return n1.v[0] > n2.v[0];}
bool operator<=(const Node& n1, const Node& n2) { return n1.v[0] <= n2.v[0];}
bool operator>=(const Node& n1, const Node& n2) { return n1.v[0] >= n2.v[0];}
bool operator==(const Node& n1, const Node& n2) { return n1.v[0] == n2.v[0];}
bool operator!=(const Node& n1, const Node& n2) { return n1.v[0] != n2.v[0];}

bool Node::compareNodePtr(Node* n1, Node* n2) {
	return *n1 < *n2;
}


void Node::addLink(Link* l) {
	if (std::find(links.begin(), links.end(), l) == links.end()) {
		links.push_back(l);
	}
}

void Node::removeLink(Link* l) {
	links.erase(std::remove(links.begin(), links.end(), l), links.end());
}



class Link{
	public:
	int id;
	std::vector<Node*> nodes;
	float chi2;
	int type;
	int flag;
	
	enum {
		NORMAL, REJECTED, MULTIPLENODES,
	};
	enum {
		EDGE, PATH
	};
	enum {
		FORWARD = 1, BACKWARD = -1
	};
	
	Link() : chi2(0), flag(NORMAL), type(EDGE) {nodes.reserve(2);}
	Link(Node* n1, Node* n2, Node* n3=NULL, Node* n4=NULL, Node* n5=NULL)
		: chi2(0), flag(0) {
		nodes.reserve(2);
		if (n1) nodes.push_back(n1);
		if (n2) nodes.push_back(n2);
		if (n3) nodes.push_back(n3);
		if (n4) nodes.push_back(n4);
		if (n5) nodes.push_back(n5);
		sort();
	}
	int addForward(Node* n1) {
		if (n1->v[0] <= nodes.back()->v[0]) return 0;
		if (std::find(nodes.begin(), nodes.end(), n1) == nodes.end()) {
			nodes.push_back(n1);
			return 1;
		}
		else return 0;
	}
	int addForward(Link* l) {
		int n = l->nodes.size();
		int sum = 0;
		for (int i = 0; i < n; i++) {
			sum += addForward(l->nodes[i]);
		}
		return sum;
	}
	
	
	int add(Node* n1) {
		if (std::find(nodes.begin(), nodes.end(), n1) == nodes.end()) {
			nodes.push_back(n1);
			if (nodes.size() >= 2) if (*nodes[nodes.size() - 2] > *n1) sort();
			return 1;
		}
		return 0;
	}
	
	int add(Link* l) {
		int n = l->nodes.size();
		int sum = 0;
		for (int i = 0; i < n; i++) {
			sum += add(l->nodes[i]);
		}
		return sum;
	}
	
	void sort() {
		std::sort(nodes.begin(), nodes.end(), Node::compareNodePtr);
	}
	
	void print() {
		int n = nodes.size();
		for (int i = 0; i < n; i++) {
			//Oscar: added printing plate ID
			printf("ID:%d,iz:%d %s", nodes[i]->ID(),nodes[i]->Plate(), i == n - 1 ? " " : "-> ");
			//original:
			//printf("%d %s", nodes[i]->ID(), i == n - 1 ? " " : "-> ");
		}
		printf("flag=%d\n", flag);
	}
	int include(Link* l) {
		for (int i = 0, ln_size = l->nodes.size(); i < ln_size; i++) {
			if (std::find(nodes.begin(), nodes.end(), l->nodes[i]) == nodes.end()) {
				// this Node is not found in this link.
				return 0;
			}
		}
		// all nodes are included in this link.
		return 1;
	}
	int include(Node* n) {
		// return if this Node is included in this link.
		if (std::find(nodes.begin(), nodes.end(), n) == nodes.end()) return 0;
		else return 1;
	}
	
	float getChi2() { return chi2;}
	void setChi2(float chi2value) { chi2 = chi2value;}
	Node* nodeFirst() { return nodes.front();}
	Node* nodeLast() { return nodes.back();}
		
};


bool operator==(const Link& l1, const Link& l2) {
	// return if this two links are same or not.
        int l1n_size = l1.nodes.size();
	if (l1n_size != l2.nodes.size()) return 0;
	else{
		for (int i = 0; i < l1n_size; i++) {
			if (l1.nodes[i] != l2.nodes[i]) return 0;
		}
	}
	return 1;
}
bool operator!=(const Link& l1, const Link& l2) {
	return !(l1 == l2);
}



class hashtable3d{
	
	public:
	
	float vmin[3];
	float vmax[3];
	float div[3];
	float n[3];
	
	std::vector<Node*> neighbors;
	std::vector< std::vector< std::vector< std::vector<Node*> > > > table;
	
	
	void setCells(int n1, float min1, float max1,
		      int n2, float min2, float max2,
		      int n3, float min3, float max3) {
		printf("setting cells\n");
		vmin[0] = min1;
		vmin[1] = min2;
		vmin[2] = min3;
		vmax[0] = max1;
		vmax[1] = max2;
		vmax[2] = max3;
		n[0] = n1;
		n[1] = n2;
		n[2] = n3;
		div[0] = (max1 - min1)/n1;
		div[1] = (max2 - min2)/n2;
		div[2] = (max3 - min3)/n3;
		
		table.resize(n1);
		for (int i = 0; i < n1; i++) {
			table[i].resize(n2);
			for (int j = 0; j < n2; j++) {
				table[i][j].resize(n3);
			}
		}
	}
	
	std::vector<Node*>& findCell(float* v) {
		int i = getCellIndex(0, v[0]);
		int j = getCellIndex(1, v[1]);
		int k = getCellIndex(2, v[2]);
		return table[i][j][k];
	}
	int getCellIndex(int iv, float v) {
		int idx = (v - vmin[iv])/div[iv];
		if (idx < 0) idx = 0;
		if (idx >= n[iv]) idx = n[iv] - 1;
		return idx;
	}
	
//	std::vector<Node*> getNeighbors(float v1, float v2, float v3, float v3, float r1, float r2, float r3, float r3) {
	std::vector<Node*>& getNeighbors(float* v, float* r) {
		int ivmin[3], ivmax[3];
		for (int i = 0; i < 3; i++) {
			ivmin[i] = getCellIndex(i, v[i] - r[i]);
			ivmax[i] = getCellIndex(i, v[i] + r[i]);
		}
		
		neighbors.clear();
		
		float vmin[3] = {v[0] - r[0], v[1] - r[1], v[2] - r[2]};
		float vmax[3] = {v[0] + r[0], v[1] + r[1], v[2] + r[2]};
		
		for (int i = ivmin[0]; i <= ivmax[0]; i++) {
		for (int j = ivmin[1]; j <= ivmax[1]; j++) {
		for (int k = ivmin[2]; k <= ivmax[2]; k++) {
			std::vector<Node*>& vec = table[i][j][k];
			for (int m = 0, vec_size = vec.size(); m < vec_size; m++) {
				Node* n = vec[m];
				if (vmin[0] < n->v[0])
				if (vmin[1] < n->v[1])
				if (vmin[2] < n->v[2])
				if (n->v[0] < vmax[0])
				if (n->v[1] < vmax[1])
				if (n->v[2] < vmax[2])
					neighbors.push_back(vec[m]);
			}
		}}}
		
		return neighbors;
	}
	
	void fillCells(std::vector<Node*>& vec) {
		printf("filling cells\n");
		for (int i = 0, vec_size = vec.size(); i < vec_size; i++) {
			std::vector<Node*>& cell = findCell(vec[i]->v);
			cell.push_back(vec[i]);
		}
	}
	
};




std::vector<Link*> Node::getLinks(int flag, int direction) {
	std::vector<Link*> vec;
	for (int i = 0, links_size = links.size(); i < links_size; i++) {
		Link* lnk = links[i];
		if (lnk->flag == flag) {
			
			if (direction == Link::FORWARD) {
				if (lnk->nodeFirst() == this) vec.push_back(lnk);
			} 
			else if (direction == Link::BACKWARD) {
				if (lnk->nodeLast() == this) vec.push_back(lnk);
			}
			else {
				vec.push_back(lnk);
			}
		}
	}
	return vec;
}
