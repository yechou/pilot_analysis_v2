#include<TFile.h>
#include<TTree.h>
#include<TBranch.h>
#include<TLeaf.h>
#include<stdlib.h>
int main(int argc,char* argv[]){
	TFile f(argv[1]);
	TTree *t = (TTree*)f.Get("FASERnu");
	TLeaf *xl = t->FindLeaf("x");
	TLeaf *yl = t->FindLeaf("y");
	TLeaf *zl = t->FindLeaf("z");
	TLeaf *pxl = t->FindLeaf("px");
	TLeaf *pyl = t->FindLeaf("py");
	TLeaf *pzl = t->FindLeaf("pz");
	TLeaf *izl = t->FindLeaf("iz");
	TLeaf *izsubl = t->FindLeaf("izsub");
	TLeaf *pdgidl = t->FindLeaf("pdgid");
	TLeaf *chargel = t->FindLeaf("charge");
	//float x,y,z,tx,ty,etruth;

	int nentries = t->GetEntries();
	Float_t x,y,z,px,py,pz,tx,ty,etruth;
	Int_t iz,izsub,pdgid,clstruth,charge;
	TFile g(argv[2],"RECREATE");
	TTree *tw[29];
	for(int i=0;i<29;i++){
		tw[i] = new TTree(Form("tree%d",i+90),Form("BTs in layer %d",i+90));
		tw[i]->Branch("x",&x,"x/F");
		tw[i]->Branch("y",&y,"y/F");
		tw[i]->Branch("z",&z,"z/F");
		tw[i]->Branch("tx",&tx,"tx/F");
		tw[i]->Branch("ty",&ty,"ty/F");
		tw[i]->Branch("pdgid",&pdgid,"pdgid/I");
		tw[i]->Branch("clstruth",&clstruth,"clstruth/I");
		tw[i]->Branch("etruth",&etruth,"etruth/F");
	}
	for(int i=0;i<nentries;i++){
		t->GetEntry(i);
		int llen = xl->GetLen();
		for(int j=0;j<llen;j++){
			iz = izl->GetValue(j);
			if(iz<72)continue;
			izsub = izsubl->GetValue(j);
			if(izsub!=0)continue;
			charge = chargel->GetValue(j);
			if(charge==0)continue;
			pdgid = pdgidl->GetValue(j);
			if(pdgid==22)continue;
			px = pxl->GetValue(j);
			pz = pzl->GetValue(j);
			tx = px/pz;
			if(tx<-2||tx>2)continue;
			py = pyl->GetValue(j);
			ty = py/pz;
			if(ty<-2||ty>2)continue;
			etruth = sqrt(px*px+py*py+pz*pz);
			if(etruth<20)continue;
			x = xl->GetValue(j);
			y = yl->GetValue(j);
			z = zl->GetValue(j);
			//Oscar: change unit from micrometer to mm
			z=z*1000;
			x=x*1000;
			y=y*1000;
			clstruth = i;
			
			//if(i%1000==0)
			printf("%d x=%f y=%f z=%f %d %f %f %d %d %f\n",i,x,y,z,iz,tx,ty,pdgid,clstruth,etruth);
			tw[iz-72]->Fill();
		}
	}
	for(int i=0;i<29;i++)tw[i]->Write();
	g.Close();
	printf("SUCCESS\n");
	f.Close();
	return 0;
}
