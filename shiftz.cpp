#include<TFile.h>
#include<TTree.h>
#include<TBranch.h>
#include<TString.h>
#include <cmath>
#include <iostream>

int main(int argc,char* argv[]){
	TFile fout(argv[2],"RECREATE");
	// This zarr2 array contains the BT z coordinates in MC generation that need to be shifted
	float zarr2[29]={-17500., -16200.00097656,
       -14900.,  -13600.,
        -12300., -11000.,
         -9700. , -8400. ,
         -7100.,  -5800.,
        -4500.,-3200.,
         -1900., -600.,
             700.,2000. ,
           3300., 4600. ,
           5900. ,  7200.,
            8500., 9800.,
          11100.,12400. ,
         13700. , 15000. ,
        16299.99902344};
        Float_t x,y,z,tx,ty,etruth;
        Int_t pdgid,clstruth;
	TTree *tw[29];
        for(int ipl=0;ipl<29;ipl++){
		tw[ipl] = new TTree(Form("tree%d",ipl+90),Form("BTs in layer %d",ipl+90));
		tw[ipl]->Branch("x",&x,"x/F");
        	tw[ipl]->Branch("y",&y,"y/F");
        	tw[ipl]->Branch("z",&z,"z/F");
        	tw[ipl]->Branch("tx",&tx,"tx/F");
        	tw[ipl]->Branch("ty",&ty,"ty/F");
        	tw[ipl]->Branch("pdgid",&pdgid,"pdgid/I");
        	tw[ipl]->Branch("clstruth",&clstruth,"clstruth/I");
        	tw[ipl]->Branch("etruth",&etruth,"etruth/F");
	}
	TFile f(argv[1]);
	for(int ipl=90;ipl<119;ipl++){
		TTree *t = (TTree*)f.Get(Form("tree%d;1",ipl));
		t->SetBranchAddress("x",&x);
		t->SetBranchAddress("y",&y);
		t->SetBranchAddress("z",&z);
		t->SetBranchAddress("tx",&tx);
		t->SetBranchAddress("ty",&ty);
		t->SetBranchAddress("pdgid",&pdgid);
		t->SetBranchAddress("clstruth",&clstruth);
		t->SetBranchAddress("etruth",&etruth);
                int nentries = t->GetEntries();
                printf("%d %d\n",ipl,nentries);
		for(int j=0;j<nentries;j++){
			t->GetEntry(j);
			for(int k=0;k<sizeof(zarr2);k++){
				if(z==zarr2[k]){
					// Shift z coordinate, and also use tx and ty to shift x and y coordinate.
					printf("before: x= %f, y= %f, z= %f\n",x,y,z);
					z = round(z) - 50;
					x = x - 50*tx;
					y = y - 50*ty;
					printf("after: x= %f, y= %f, z= %f\n",x,y,z);
				}
			}
			tw[ipl-90]->Fill();
			if(j%1000==0)printf("%d / %d %f %f %f %f %f %d %d %f\n",j,nentries,x,y,z,tx,ty,pdgid,clstruth,etruth);
		}
	}
	f.Close();
	
	fout.cd();
	for(int ipl=0;ipl<29;ipl++)tw[ipl]->Write();
	printf("SUCCESS\n");	
}
