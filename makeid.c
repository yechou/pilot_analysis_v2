#include<TFile.h>
#include<TTree.h>
#include<TBranch.h>
#include<TString.h>
#include<stdlib.h>
#include<iostream>
using namespace std;
void makeid(TString in,TString out){
	TFile f(in);
	TFile g(out,"RECREATE");
	TTree *tw[29];
	float x,y,z,tx,ty,etruth;
	int id,pdgid,clstruth;
	for(int i=0;i<29;i++){
		tw[i] = new TTree(Form("tree%d",i+90),Form("BTs in layer %d",i+90));
		tw[i]->Branch("x",&x,"x/F");
		tw[i]->Branch("y",&y,"y/F");
		tw[i]->Branch("z",&z,"z/F");
		tw[i]->Branch("id",&id,"id/I");
		tw[i]->Branch("tx",&tx,"tx/F");
		tw[i]->Branch("ty",&ty,"ty/F");
		tw[i]->Branch("pdgid",&pdgid,"pdgid/I");
		tw[i]->Branch("clstruth",&clstruth,"clstruth/I");
		tw[i]->Branch("etruth",&etruth,"etruth/F");
	}
	for(int i=0;i<29;i++){
		TTree *t = (TTree*)f.Get(Form("tree%d",i+90));
		t->SetBranchAddress("x",&x);
		t->SetBranchAddress("y",&y);
		t->SetBranchAddress("z",&z);
		t->SetBranchAddress("tx",&tx);
		t->SetBranchAddress("ty",&ty);
		t->SetBranchAddress("pdgid",&pdgid);
		t->SetBranchAddress("clstruth",&clstruth);
		t->SetBranchAddress("etruth",&etruth);
		int nentries = t->GetEntries();
		for(int j=0;j<nentries;j++){
			t->GetEntry(j);
			id = j;
			if(j%10000==0)cout << id << " " << x << " " << y << " " << z << " " << tx << " " << ty << " " << pdgid << " " << clstruth << " " << etruth << endl;
			tw[i]->Fill();
		}
		
	}
	for(int i=0;i<29;i++){
		tw[i]->Write();
	}
	g.Close();
	f.Close();
}
